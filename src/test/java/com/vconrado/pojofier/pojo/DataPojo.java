package com.vconrado.pojofier.pojo;

import com.vconrado.pojofier.annotation.PojoField;
import com.vconrado.pojofier.converter.ChildPojoConverter;
import com.vconrado.pojofier.converter.ElapsedTimeConverter;
import com.vconrado.pojofier.converter.IntegerToStringConverter;
import java.util.List;

/**
 *
 * @author Vitor Gomes
 * @email vitor@ieav.cta.br
 */
public class DataPojo {

    // Sem nenhuma anotação, o campo tem q ter o mesmo nome da classe fonte
    Long id;
    // É possível trocar o nome, informando através da anotação @PojoField
    @PojoField("name")
    String theName;
    // é possível indicar um conversor customizado para o campo. Neste caso, simplesmente converte Integer para String.
    @PojoField(converter = IntegerToStringConverter.class)
    String idade;
    // É possível trocar o nome e indicar um conversor.
    // No caso deste conversor, ele calcula o tempo passado em Anos, meses e dias da data passada.
    // O retorno é uma String.
    @PojoField(value = "updated", converter = ElapsedTimeConverter.class)
    String ultimoAcesso;
    @PojoField(converter = ChildPojoConverter.class)
    List<ChildPojo> childs;

    public String getTheName() {
        return theName;
    }

    public void setTheName(String theName) {
        this.theName = theName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getUltimoAcesso() {
        return ultimoAcesso;
    }

    public void setUltimoAcesso(String ultimoAcesso) {
        this.ultimoAcesso = ultimoAcesso;
    }

    public List<ChildPojo> getChilds() {
        return childs;
    }

    public void setChilds(List<ChildPojo> childs) {
        this.childs = childs;
    }

    @Override
    public String toString() {
        return "DataPojo{" + "id=" + id + ", theName=" + theName + ", idade=" + idade + ", ultimoAcesso=" + ultimoAcesso + ", childs=" + childs + '}';
    }

}
