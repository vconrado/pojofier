package com.vconrado.pojofier.converter;

import com.vconrado.pojofier.data.Event;

/*
 *  *****************************************************************************
 *                    Lean Survey Pesquisas de Mercado S.A
 *  -----------------------------------------------------------------------------
 http://www.leansurvey.com.br
 *                      Contato: <dev@leansurvey.com.br>
 *  *****************************************************************************
 */
public class EventToDelayConverter implements IPojoConverter<Event, Long> {

    @Override
    public Long convert(Event p) {
        if (p == null) {
            return (long) 0;
        }
        return (p.getEnd().getTime() - p.getStart().getTime());
    }

}
