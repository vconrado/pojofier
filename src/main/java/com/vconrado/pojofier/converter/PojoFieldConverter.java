package com.vconrado.pojofier.converter;

/**
 *
 * @author Vitor Gomes
 * @param <P> Param type to convert method
 * @param <R> Return type to convert method
 */
public class PojoFieldConverter<P, R> implements IPojoConverter<P, R> {

    @Override
    public R convert(P p) {
        return (R) p;
    }

}
